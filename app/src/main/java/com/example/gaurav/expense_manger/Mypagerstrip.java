package com.example.gaurav.expense_manger;

import android.content.Context;
import android.support.v4.view.PagerTitleStrip;
import android.util.AttributeSet;
import android.view.ViewManager;
import android.widget.TextView;

public class Mypagerstrip extends PagerTitleStrip {
    public Mypagerstrip(Context context, AttributeSet attrs) {
        super(context, attrs);
        ((ViewManager) (this.getChildAt(2)).getParent()).removeView((TextView)this.getChildAt(2));
        ((ViewManager) (this.getChildAt(0)).getParent()).removeView((TextView)this.getChildAt(0));
    }

}