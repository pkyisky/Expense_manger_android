package com.example.gaurav.expense_manger;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gaurav.expense_manger.Adapter.Autocomplete_adapter;
import com.example.gaurav.expense_manger.Adapter.horizontalviewadapter;
import com.example.gaurav.expense_manger.Backend.Backendserver;
import com.example.gaurav.expense_manger.entities.Item;
import com.example.gaurav.expense_manger.entities.acounts_item;
import com.example.gaurav.expense_manger.entities.horizontal_item;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Upload extends AppCompatActivity {
    ImageView imageView;
    AutoCompleteTextView titleet;
    LinearLayout layout1;
    TextView balance,imagename;
    TextView erroruploadtitle,erroruploadamount,erroruploaddesc;
    EditText amount,desc;
    Button submit1;
    Bitmap bitmap,bitmap1;
    String cam;
    LinearLayout imagelayout;
    private AsyncHttpClient client;
    ArrayList<String>spinerlist;
    ArrayList<Item>titlelist;
    ArrayList<acounts_item>accountlist;
    boolean shouldAddSpace = false;
    String path="", base64="";
    Spinner spinner;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1, choose;
    Float balanceleft;
    String spinvalue,title,pkheading,result;

    String projectpk="",amountsting,descstring,acountspk,projcampk="";
    ArrayAdapter<String> spinnerAdapter;
    Autocomplete_adapter adapter1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        imageView=findViewById(R.id.image);
        titleet=findViewById(R.id.titleet);
        amount=findViewById(R.id.amount);
        desc=findViewById(R.id.desc);
        imagename=findViewById(R.id.imagename);
        balance=findViewById(R.id.balance);
        submit1=findViewById(R.id.submit1);

        erroruploadtitle=findViewById(R.id.erroruploadtitle);
        erroruploadamount=findViewById(R.id.erroruploadamnt);
        erroruploaddesc=findViewById(R.id.erroruploaddesc);


        spinerlist=new ArrayList<>();
        titlelist=new ArrayList<>();
        accountlist=new ArrayList<>();
        spinner=(Spinner)findViewById(R.id.spinner1);

        Backendserver backend = new Backendserver(getApplicationContext());
        client = backend.getHTTPClient();
        Bundle bundle = getIntent().getExtras();

        try {
            projcampk=bundle.getString("projectpkey");
            projectpk = bundle.getString("projectpk");

        }
        catch (Exception e){
           e.printStackTrace();
        }

        path=bundle.getString("data");

        if(path!=null)
        {
            imagename.setTextColor(Color.parseColor("#008000"));
            imagename.setText("Image attached");
        }

      //  Toast.makeText(getApplicationContext(),"projectpk "+projectpk,Toast.LENGTH_SHORT).show();
      //  Toast.makeText(getApplicationContext(),"projcampk "+projcampk,Toast.LENGTH_SHORT).show();
        gettitle();
        getacounts();
        initializeUI();
        Log.e("error",""+path);
        Log.e("error",""+projcampk);
        Log.e("error",""+projectpk);

        imagelayout=(LinearLayout) findViewById(R.id.choose_image);

        titleet.setThreshold(1);
        titleet.setDropDownHeight(200);
        //edittext.setDropDownVerticalOffset(300);
        adapter1 = new Autocomplete_adapter(getApplicationContext(), R.layout.activity_upload, R.id.lbl_name, titlelist);


       /* titleet.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (shouldAddSpace && text.length() > 0 && !text.endsWith(" ")) {
                    s.append(' ');
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        }); */

        titleet.setAdapter(adapter1);

        submit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(getApplicationContext(),"success item added",Toast.LENGTH_SHORT).show();
                tittle();

            }
        });
        imagelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryIntent();
            }
        });

        bitmap1=(Bitmap) getIntent().getParcelableExtra("bitmap");

    }


    private void initializeUI() {


        spinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> arg0,
                                               View arg1, int position, long arg3) {


                     // ((TextView) arg1).setTextColor(Color.RED);

                        String selectedDiv = arg0.getItemAtPosition(position).toString();

                        String mySelected = spinner.getSelectedItem().toString();
                        //spinner.setSelection(Integer.parseInt(selectedDiv));

                        acountspk=accountlist.get(position).getPk();
                       // Toast.makeText(Upload.this, ""+selectedDiv+""+acountspk
                          //      , Toast.LENGTH_LONG).show();
                        balanceleft=Float.parseFloat(accountlist.get(position).getBalance());

                        if(Float.parseFloat(accountlist.get(position).getBalance())==0)
                        {
                            balance.setTextColor((Color.parseColor("#ff0000")));
                            balance.setText("  Balance : Rs "+accountlist.get(position).getBalance().toString());

                        }
                        else{
                            balance.setTextColor((Color.parseColor("#008000")));
                            balance.setText("  Balance : Rs "+accountlist.get(position).getBalance().toString());
                        }





                        // Locate the textviews in activity_main.xml
                    //    TextView txtrank = (TextView) findViewById(R.id.rank);
                    //    TextView txtcountry = (TextView) findViewById(R.id.country);
                   //      TextView txtpopulation = (TextView) findViewById(R.id.population);

                        // Set the text followed by the position
                 //       txtrank.setText("Rank : "
                    //            + world.get(position).getRank());
                  //      txtcountry.setText("Country : "
                  //              + world.get(position).getCountry());
                   //     txtpopulation.setText("Population : "
                      //          + world.get(position).getPopulation());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                    }
                });


       /* ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, spinerlist);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter); */

    }


    public void gettitle(){

        client.get( Backendserver.url+ "/api/finance/expenseHeading/",new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);

                //        Toast.makeText(getApplicationContext(),"res "+response.toString(),Toast.LENGTH_SHORT).show();
                for (int i = 0; i < response.length(); i++) {

                    JSONObject object=null;

                    try {
                        object = response.getJSONObject(i);

                        titlelist.add(new Item(object));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getApplicationContext(),"data not fetching"+errorResponse,Toast.LENGTH_SHORT);
            }
        });

    }


    public void getacounts(){

        client.get( Backendserver.url+ "/api/finance/account/?&personal=true&contactPerson="+MainActivity.userPK,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {


                //        Toast.makeText(getApplicationContext(),"res "+response.toString(),Toast.LENGTH_SHORT).show();
                for (int i = 0; i < response.length(); i++) {

                    JSONObject object=null;

                    try {
                        object = response.getJSONObject(i);

                        String spiner = object.getString("title");



                        spinerlist.add(spiner);

                        accountlist.add(new acounts_item(object));

                    }


                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                spinnerAdapter = new ArrayAdapter<String>(Upload.this, android.R.layout.simple_spinner_item, spinerlist);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setSelection(spinnerAdapter.getPosition("select"));
                spinner.setAdapter(spinnerAdapter);
                spinnerAdapter.notifyDataSetChanged();



            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getApplicationContext(),"data not fetching"+errorResponse,Toast.LENGTH_SHORT);
            }
        });

    }





public void tittle(){

        if(desc.length() == 0&&amount.length() == 0&&titleet.length() == 0)
    {
        erroruploaddesc.setTextColor(Color.parseColor("#ff0000"));
        erroruploaddesc.setText("Description is required.");
        erroruploadamount.setTextColor(Color.parseColor("#ff0000"));
        erroruploadamount.setText("Amount is required.");
        erroruploadtitle.setTextColor(Color.parseColor("#ff0000"));
        erroruploadtitle.setText("Title is required.");
        // desc.setError("Enter amount");
    }
    if(desc.length() == 0&&amount.length() == 0)
    {
        erroruploadamount.setVisibility(View.VISIBLE);
        erroruploaddesc.setVisibility(View.VISIBLE);
        erroruploaddesc.setTextColor(Color.parseColor("#ff0000"));
        erroruploaddesc.setText("Description is required.");
        erroruploadamount.setTextColor(Color.parseColor("#ff0000"));
        erroruploadamount.setText("Amount is required.");

        // desc.setError("Enter amount");
    }

   if(desc.length() == 0&&amount.length()>0)
    {
        erroruploadamount.setVisibility(View.GONE);
        erroruploaddesc.setVisibility(View.VISIBLE);
        erroruploaddesc.setTextColor(Color.parseColor("#ff0000"));
        erroruploaddesc.setText("Description is required.");
        // desc.setError("Enter amount");
    }
   if(desc.length()> 0&&amount.length()==0)
    {
        erroruploaddesc.setVisibility(View.GONE);
        erroruploadamount.setVisibility(View.VISIBLE);
        erroruploadamount.setTextColor(Color.parseColor("#ff0000"));
        erroruploadamount.setText("Amount is required.");
        // desc.setError("Enter amount");
    }



    if (titleet.length() == 0) {
            erroruploadtitle.setVisibility(View.VISIBLE);
        erroruploadtitle.setTextColor(Color.parseColor("#ff0000"));
        erroruploadtitle.setText("Title is required.");
     //   titleet.setError("Enter title");
    } else {
        erroruploadtitle.setVisibility(View.GONE);
        result = titleet.getText().toString();

        for (int i = 0; i < titlelist.size(); i++) {
            pkheading = titlelist.get(i).getPk();
            title = titlelist.get(i).getTitle1();

            // Toast.makeText(getApplicationContext()," "+partnobom,Toast.LENGTH_SHORT).show();

            if (title.equals(result)) {
                Log.e("matched", title + " " + pkheading);
                break;
            }

        }

        if(title.equals(result)){
            Log.e("matched", title + " " + pkheading);

            if (amount.length() == 0) {
                erroruploadamount.setTextColor(Color.parseColor("#ff0000"));
                erroruploadamount.setText("Amount is required.");
               // amount.setError("Enter amount");
            }else if(Integer.parseInt(amount.getText().toString())> 20000)
            {
                Toast.makeText(getApplicationContext(),"Amount must be less than : 20000",Toast.LENGTH_SHORT).show();
            }
            else if( Float.parseFloat(amount.getText().toString())>balanceleft)
            {
                Toast.makeText(getApplicationContext(),"Insufficient account balance",Toast.LENGTH_SHORT).show();
            }
            else if( Integer.parseInt(amount.getText().toString())==0)
            {
                Toast.makeText(getApplicationContext(),"Please enter valid amount",Toast.LENGTH_SHORT).show();
            }
            else if(desc.length() == 0)
            {
                erroruploaddesc.setTextColor(Color.parseColor("#ff0000"));
                erroruploaddesc.setText("Description is required.");
               // desc.setError("Enter amount");
            }

            else if(amount.length()>0&&desc.length()>0&&pkheading.length()>0&&projcampk==null&&bitmap!=null)
            {
                Log.e("error","project pk");
                postform();
            }
            else if(amount.length()>0&&desc.length()>0&&pkheading.length()>0&&projectpk==null&&bitmap1!=null)
            {
                Log.e("error","project camp pk");
                postformcam();
            }
            else if(amount.length()>0&&desc.length()>0&&pkheading.length()>0&&projectpk==null&&bitmap==null)
            {
                Log.e("error","project camp pk");
                postformcamwithoutbit();
            }
            else if(amount.length()>0&&desc.length()>0&&pkheading.length()>0&&projcampk==null&&bitmap1==null)
            {
                Log.e("error","project pk");

                postwithoutattachment();
            }





        }
        else {
            if (desc.length() > 0 && amount.length() > 0 && titleet.length() > 0) {
                RequestParams params = new RequestParams();
                params.put("title", result);
                client.post(Backendserver.url + "/api/finance/expenseHeading/", params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Toast.makeText(getApplicationContext(), "Title added", Toast.LENGTH_SHORT).show();
                        String str = null;
                        try {


                            str = new String(responseBody, "UTF-8");
                            Log.e("sucess", "sucees" + " " + str);
                            JSONObject obj = new JSONObject(str);

                            if (amount.length() == 0) {
                                erroruploadamount.setTextColor(Color.parseColor("#ff0000"));
                                erroruploadamount.setText("Amount is required.");
                                //  amount.setError("Enter amount");
                            } else if (Integer.parseInt(amount.getText().toString()) > 20000) {
                                Toast.makeText(getApplicationContext(), "Amount must be less than : 20000", Toast.LENGTH_SHORT).show();
                            } else if (Float.parseFloat(amount.getText().toString()) > balanceleft) {
                                Toast.makeText(getApplicationContext(), "Insufficient account balance", Toast.LENGTH_SHORT).show();
                            } else if (Integer.parseInt(amount.getText().toString()) == 0) {
                                Toast.makeText(getApplicationContext(), "Please enter valid amount", Toast.LENGTH_SHORT).show();
                            } else if (desc.length() == 0) {
                                erroruploaddesc.setTextColor(Color.parseColor("#ff0000"));
                                erroruploaddesc.setText("Description is required.");
                                //  desc.setError("Enter amount");
                            } else if (amount.length() > 0 && desc.length() > 0 && pkheading.length() > 0 && projcampk == null&&bitmap!=null) {
                                Log.e("error", "project pk");
                                postform();
                            } else if (amount.length() > 0 && desc.length() > 0 && pkheading.length() > 0 && projectpk == null&&bitmap1!=null) {
                                Log.e("error", "project camp pk");
                                postformcam();
                            }
                            else if(amount.length()>0&&desc.length()>0&&pkheading.length()>0&&projectpk==null&&bitmap==null)
                            {
                                Log.e("error","project camp pk");
                                postformcamwithoutbit();
                            }
                            else if(amount.length()>0&&desc.length()>0&&pkheading.length()>0&&projcampk==null&&bitmap1==null)
                            {
                                Log.e("error","project pk");
                                postwithoutattachment();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Toast.makeText(getApplicationContext(), "unsuccess ", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        }

    }
}



    public void postform(){

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
            byte[] image = output.toByteArray();

            RequestParams params = new RequestParams();
            params.put("project", Integer.parseInt(projectpk));
            params.put("amount", Integer.parseInt(amount.getText().toString()));
            params.put("account", Integer.parseInt(acountspk));
            params.put("description", desc.getText().toString());
            params.put("heading", Integer.parseInt(pkheading));
            params.put("attachment", new ByteArrayInputStream(image), path);
            params.put("createdUser", "1");
            Log.e("error", "" + params);
            client.post(Backendserver.url + "/api/projects/pettyCash/", params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Toast.makeText(getApplicationContext(), "success item added", Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    // Add new Flag to start new Activity
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(i);
                    finish();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(getApplicationContext(), "unsuccess ", Toast.LENGTH_SHORT).show();
                    Log.e("error", "" + error);

                }
            });


    }

    public void postwithoutattachment(){

        RequestParams params = new RequestParams();
        params.put("project", Integer.parseInt(projectpk));
        params.put("amount", Integer.parseInt(amount.getText().toString()));
        params.put("account", Integer.parseInt(acountspk));
        params.put("description", desc.getText().toString());
        params.put("heading", Integer.parseInt(pkheading));
     //   params.put("attachment", new ByteArrayInputStream(image), path);
        params.put("createdUser", "1");
        Log.e("error", "" + params);
        client.post(Backendserver.url + "/api/projects/pettyCash/", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Toast.makeText(getApplicationContext(), "success item added", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);
                finish();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "unsuccess ", Toast.LENGTH_SHORT).show();
                Log.e("error", "" + error);

            }
        });
    }

 /*   public void post(){

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
        byte[] image = output.toByteArray();

        RequestParams params = new RequestParams();
        params.put("project", Integer.parseInt(projectpk));
        params.put("amount", Integer.parseInt(amount.getText().toString()));
        params.put("account", Integer.parseInt(acountspk));
        params.put("description", desc.getText().toString());
        params.put("heading", Integer.parseInt(pkheading));
        params.put("attachment", new ByteArrayInputStream(image), path);
        params.put("createdUser", "1");
        Log.e("error", "" + params);
        client.post(Backendserver.url + "/api/projects/pettyCash/", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Toast.makeText(getApplicationContext(), "success item added", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);
                finish();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "unsuccess ", Toast.LENGTH_SHORT).show();
                Log.e("error", "" + error);

            }
        });
    } */



    public void postformcam(){

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, output);
            byte[] image = output.toByteArray();

            RequestParams params = new RequestParams();
            params.put("project", Integer.parseInt(projcampk));
            params.put("amount", Integer.parseInt(amount.getText().toString()));
            params.put("account", Integer.parseInt(acountspk));
            params.put("description", desc.getText().toString());
            params.put("heading", Integer.parseInt(pkheading));
            params.put("attachment", new ByteArrayInputStream(image), path);
            params.put("createdUser", "1");
            Log.e("error", "" + params);
            client.post(Backendserver.url + "/api/projects/pettyCash/", params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Toast.makeText(getApplicationContext(), "success item added", Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    // Add new Flag to start new Activity
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(i);
                    finish();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(getApplicationContext(), "unsuccess ", Toast.LENGTH_SHORT).show();
                    Log.e("error", "" + error);

                }
            });

    }
    public void postformcamwithoutbit(){

        RequestParams params = new RequestParams();
        params.put("project", Integer.parseInt(projcampk));
        params.put("amount", Integer.parseInt(amount.getText().toString()));
        params.put("account", Integer.parseInt(acountspk));
        params.put("description", desc.getText().toString());
        params.put("heading", Integer.parseInt(pkheading));
       // params.put("attachment", new ByteArrayInputStream(image), path);
        params.put("createdUser", "1");
        Log.e("error", "" + params);
        client.post(Backendserver.url + "/api/projects/pettyCash/", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Toast.makeText(getApplicationContext(), "success item added", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);
                finish();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "unsuccess ", Toast.LENGTH_SHORT).show();
                Log.e("error", "" + error);

            }
        });

    }
  /*  public void postcam(){


        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, output);
        byte[] image = output.toByteArray();

        RequestParams params = new RequestParams();
        params.put("project", Integer.parseInt(projcampk));
        params.put("amount", Integer.parseInt(amount.getText().toString()));
        params.put("account", Integer.parseInt(acountspk));
        params.put("description", desc.getText().toString());
        params.put("heading", Integer.parseInt(pkheading));
        params.put("attachment", new ByteArrayInputStream(image), path);
        params.put("createdUser", "1");
        Log.e("error", "" + params);
        client.post(Backendserver.url + "/api/projects/pettyCash/", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Toast.makeText(getApplicationContext(), "success item added", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(i);
                finish();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "unsuccess ", Toast.LENGTH_SHORT).show();
                Log.e("error", "" + error);

            }
        });


    } */

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE){
                onSelectFromGalleryResult(data);
                Log.e("onActivity result", "document gallery");

            }
         //   else if (requestCode == REQUEST_CAMERA)
            //    onCaptureImageResult(data);
       // }else if(requestCode == READ_REQUEST_CODE){
      //      onCaptureDocumentFormat(data);

      //  }
    }

}
    private String bitmapToBase64(Bitmap bitmap) {
        byte[] byteArray = new byte[0];
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 65, byteArrayOutputStream);
            byteArray = byteArrayOutputStream.toByteArray();

        }catch (Exception e){
            e.printStackTrace();
        }
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    private void onSelectFromGalleryResult(Intent data) {


        if (data != null) {
            imagename.setTextColor(Color.parseColor("#008000"));
            imagename.setText("Image attached");
            path = data.getData().getPath();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("onSelectFromGalleryRes",""+path);
            Toast.makeText(getApplicationContext(), ""+path, Toast.LENGTH_SHORT).show();


        }
          //image = bitmapToBase64(bitmap);
      //  sendMessage(path);
    }


    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
      //  sendMessage(destination.getAbsolutePath());

        base64 = bitmapToBase64(bitmap);
        Toast.makeText(getApplicationContext(), ""+destination.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        Log.e("onCaptureImageResult",""+destination.getAbsolutePath());
    }


}
