package com.example.gaurav.expense_manger.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gaurav.expense_manger.Interface.Itemclicklistner;
import com.example.gaurav.expense_manger.MainActivity;
import com.example.gaurav.expense_manger.R;
import com.example.gaurav.expense_manger.Upload;
import com.example.gaurav.expense_manger.common.common;
import com.example.gaurav.expense_manger.entities.horizontal_item;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class horizontalviewadapter extends RecyclerView.Adapter<horizontalviewadapter.Holderview> {
    private ArrayList<horizontal_item> Grnlist;
    private Context context;
    Context mContext;
    public int projectpk=0;
    int rowindex=-1;
    public horizontalviewadapter(ArrayList<horizontal_item> Grnlist, Context context) {
        this.Grnlist = Grnlist;
        this.context = context;
    }
    @Override
    public horizontalviewadapter.Holderview onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.cardview,parent,false);
        mContext=parent.getContext();

        return new horizontalviewadapter.Holderview(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull Holderview holder, final int position) {
        horizontal_item grn_item=Grnlist.get(position);
        holder.v_name.setText(" "+grn_item.getTitle());
        holder.desc.setText(" "+grn_item.getDesc());
        holder.totalcost.setText(" Total Cost : "+grn_item.getTotalcost());
        holder.cardpk.setText("# "+grn_item.getPk());
      /*holder.servicename.setText(" "+grn_item.getName());
        holder.mobile.setText(" "+grn_item.getMobile());
        holder.date.setText(" "+grn_item.getDate());
        holder.status.setText(" "+grn_item.getStatus()); */
       // common.currentitem=Grnlist.get(position);
        projectpk=Integer.parseInt(Grnlist.get(position).getPk());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Uri file = Uri.fromFile(getOutputMediaFile());
                common.currentitem=Grnlist.get(position);
                notifyDataSetChanged();
                projectpk=Integer.parseInt(Grnlist.get(position).getPk());


                ((MainActivity)mContext).startActivityForResult(i,911);

            }
        });

        holder.setItemclicklistner(new Itemclicklistner() {
            @Override
            public void onClick(View view, int position) {
                context.startActivity(new Intent(context, Upload.class).putExtra("projectpk", Grnlist.get(position).getPk()));
            }
        });


      /*  holder.image.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Uri file = Uri.fromFile(getOutputMediaFile());
                 i.putExtra("projectpkey", Grnlist.get(position).getPk().toString());

                 ((MainActivity)mContext).startActivityForResult(i,911);

             }
         });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



             //   Toast.makeText(context, "click on " + Grnlist.get(position).getPk(),
                      //  Toast.LENGTH_SHORT).show();
                context.startActivity(new Intent(context, Upload.class).putExtra("projectpk", Grnlist.get(position).getPk()));

            }
        }); */




    }

    public void setfilter(ArrayList<horizontal_item>listitem)
    {

       /* if(listitem==null)
        {
            imageView.setVisibility(View.VISIBLE);
            notifyDataSetChanged();

        }*/
        Grnlist = new ArrayList<>();
        Grnlist.addAll(listitem);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return Grnlist.size();
    }



    class Holderview extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        Itemclicklistner itemclicklistner;

        TextView v_name,desc,totalcost,cardpk;
        ImageView image;
        File file;
        Holderview(View itemview)
        {
            super(itemview);
            desc=itemview.findViewById(R.id.projdesc);
            totalcost=itemview.findViewById(R.id.totalprocost);
            v_name = (TextView) itemView.findViewById(R.id.projtitle);
            image=itemview.findViewById(R.id.cameraico);
            cardpk=itemview.findViewById(R.id.cardpk);
            itemview.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            itemclicklistner.onClick(v,getAdapterPosition());
        }

        public void setItemclicklistner(Itemclicklistner itemclicklistner) {
            this.itemclicklistner = itemclicklistner;
        }

    }


}
