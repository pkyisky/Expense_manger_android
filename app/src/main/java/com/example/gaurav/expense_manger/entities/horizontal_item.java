package com.example.gaurav.expense_manger.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class horizontal_item {


    JSONObject object;

    public horizontal_item(JSONObject object) throws JSONException {
        this.object = object;
        this.pk = object.getString("pk");
        this.totalcost=object.getString("totalCost");
        this.title = object.getString("title");
        this.desc = object.getString("description");
    }

    public String getTitle() {
        return title;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setTitle(String title) {
        this.title = title;
    }

        public String title;

    public String getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(String totalcost) {
        this.totalcost = totalcost;
    }

         private String pk;
        public String desc;
        public String totalcost;
}
