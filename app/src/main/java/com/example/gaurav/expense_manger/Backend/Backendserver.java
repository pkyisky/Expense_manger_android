package com.example.gaurav.expense_manger.Backend;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;

public class Backendserver {
    public static String url = "https://srisan.cioc.in/";

    public Context context;
    SessionManager sessionManager;
    AsyncHttpClient client;

    public Backendserver(Context context){
        this.context = context;
    }

    public AsyncHttpClient getHTTPClient(){
        sessionManager = new SessionManager(this.context);
        final String csrftoken = sessionManager.getCsrfId();
        final String sessionid = sessionManager.getSessionId();

        client = new AsyncHttpClient(true, 80,443);
        client.addHeader("Referer",url);

        if (sessionid.length()>csrftoken.length()) {
            client.addHeader("X-CSRFToken" , sessionid);
            client.addHeader("COOKIE", String.format("csrftoken=%s; sessionid=%s", sessionid, csrftoken));
        } else {
            client.addHeader("X-CSRFToken" , csrftoken);
            client.addHeader("COOKIE", String.format("csrftoken=%s; sessionid=%s", csrftoken, sessionid));
        }
        return client;
    }



}
