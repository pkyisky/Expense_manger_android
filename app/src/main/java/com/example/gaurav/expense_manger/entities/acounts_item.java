package com.example.gaurav.expense_manger.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class acounts_item {

    public String pk;
    public String title;
    public String number;
    public String ifsc;




    JSONObject object;

    public acounts_item(JSONObject object) throws JSONException {
        this.object = object;
        this.pk = object.getString("pk");
        this.number=object.getString("number");
        this.title = object.getString("title");
        this.ifsc = object.getString("ifsc");
        this.bank = object.getString("bank");
        this.bankAddress = object.getString("bankAddress");
        this.balance = object.getString("balance");
    }


    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String bank;
    public String bankAddress;
    public String balance;



}
