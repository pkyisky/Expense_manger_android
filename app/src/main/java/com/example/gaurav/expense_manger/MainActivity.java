package com.example.gaurav.expense_manger;

import android.app.DatePickerDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.gaurav.expense_manger.Adapter.horizontalviewadapter;
import com.example.gaurav.expense_manger.Adapter.reportadapter;
import com.example.gaurav.expense_manger.Backend.Backendserver;
import com.example.gaurav.expense_manger.Backend.SessionManager;
import com.example.gaurav.expense_manger.common.common;
import com.example.gaurav.expense_manger.entities.horizontal_item;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.view.View.OnClickListener;
import android.widget.Toast;

import cz.msebera.android.httpclient.Header;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class MainActivity extends AppCompatActivity implements OnClickListener {
    private AsyncHttpClient client;
    TextView username1;
    ImageView btn_logout,newprojbtn,userimage;
    Button upload;
    AlertDialog.Builder builder;
    SessionManager sessionManager;
    public static String username = "";
    public static String userPK = "";
    private reportadapter adapterViewPager;

    LinearLayout layoutfrom,layoutto;

    private static Context mContext;

    private TextView fromDateEtxt;
    private TextView toDateEtxt;
    horizontalviewadapter adapter;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    CardView card;
    ArrayList <horizontal_item>list;
    ImageView camera;
    RecyclerView listshow;

    String defaultfrmdate,defaulttodate;

    String fromdateval,todateval;

    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat dateFormatter1;
    public String projectpk;

    private static final int CONTENT_VIEW_ID = 10101010;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // upload = (Button) findViewById(R.id.btn_upload);
        list=new ArrayList<>();
        listshow=findViewById(R.id.listshow);
        Backendserver backend = new Backendserver(getApplicationContext());
        client = backend.getHTTPClient();
        newprojbtn = findViewById(R.id.newprobtn);
        username1 = findViewById(R.id.usernametxt);
        btn_logout = findViewById(R.id.btn_logout);
        userimage=findViewById(R.id.userimage);
        sessionManager = new SessionManager(getApplicationContext());
        mContext = this;
        checklogin();
        getUserDetails();
      //  projectpk = Integer.parseInt(common.currentitem.getPk());
     //   upload();
        getproject();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        dateFormatter1 = new SimpleDateFormat("MM-yyyy", Locale.US);
        findViewsById();


        setDateTimeField();
        fromDateEtxt.setText("01-"+dateFormatter1.format(new Date()));
        toDateEtxt.setText(dateFormatter.format(new Date()));

        defaultfrmdate=dateFormatter1.format(new Date());
        defaulttodate=dateFormatter.format(new Date());
        android.app.FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragment_content hello = new fragment_content();
        fragmentTransaction.add(R.id.frame, hello, "HELLO");
        fragmentTransaction.commit();



        builder = new AlertDialog.Builder(this);
        btn_logout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //Uncomment the below code to Set the message and title from the strings.xml file
                //builder.setMessage("jsdjvsd") .setTitle("sjbasjcb");
                //Setting message manually and performing action on button click
                builder.setMessage("Do you want to Log out ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sessionManager.logoutUser();
                                finish();

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Log out");
                alert.show();
            }


        });
        Log.e("error", "fsdfsdf");
        LinearLayout gallery = findViewById(R.id.gallery);
        LayoutInflater inflater = LayoutInflater.from(this);

      //  View view = inflater.inflate(R.layout.item, gallery, false);

     //   imageView = view.findViewById(R.id.image);

      /*  for (int i = 0; i < 6; i++) {

            View view = inflater.inflate(R.layout.item, gallery, false);

            card = view.findViewById(R.id.image);
            card.setOnClickListener(this);
            gallery.addView(view);
            Log.e("error", "fsdfsdf");

        } */

        if (ContextCompat.checkSelfPermission(MainActivity.this,CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[] { CAMERA, WRITE_EXTERNAL_STORAGE }, 0);
        }


        newprojbtn.setOnClickListener(this);


    }



    public void checklogin() {
        // Check login status
        if (sessionManager.getUsername().equals("")) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(this, Login.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            this.startActivity(i);
            finish();
        }

    }

    public void getUserDetails() {
        //  btn_logout.setVisibility(View.GONE);
        //  deliveryAction.setVisibility(View.GONE);
        //  btn_logout.setVisibility(View.GONE);

        client.get(Backendserver.url + "/api/HR/users/?mode=mySelf&format=json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.e("MainActivity", "onSuccess");
                super.onSuccess(statusCode, headers, response);
                // btn_logout.setVisibility(View.VISIBLE);
                // deliveryAction.setVisibility(View.VISIBLE);
                // username1.setVisibility(View.VISIBLE);
                try {

                    JSONObject usrObj = response.getJSONObject(0);
                    userPK = usrObj.getString("pk");
                    username = usrObj.getString("username");
                    String firstName = usrObj.getString("first_name");
                    String lastName = usrObj.getString("last_name");
                    //    String email = usrObj.getString("email");
                    JSONObject profileObj = usrObj.getJSONObject("profile");

                    String dpLink = profileObj.getString("displayPicture");
                    Log.e("error",""+dpLink);
                    if (dpLink.equals("null") || dpLink == null) {
                        dpLink = Backendserver.url + "static/images/userIcon.png";
                    }
                    String mobile = profileObj.getString("mobile");
                    username1.setText(firstName + " " + lastName);

                    Uri uri = Uri.parse(dpLink);

                    Picasso.with(mContext).load(uri).resize(90,90).into(userimage);
                   // userimage.setImageURI(null);
                  //  userimage.setImageURI(uri);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //getCartCount();
                //Toast.makeText(getApplicationContext(),"cartSize",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.e("MainActivity", "onFailure");
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Log.e("MainActivity", "onFinish");
            }
        });
    }

    private void findViewsById() {
        fromDateEtxt = findViewById(R.id.etxt_fromdate);
        fromDateEtxt.setInputType(InputType.TYPE_NULL);

        fromDateEtxt.requestFocus();

        toDateEtxt = findViewById(R.id.etxt_todate);
        toDateEtxt.setInputType(InputType.TYPE_NULL);

        layoutfrom=this.findViewById(R.id.layoutoutfrom);
        layoutto=this.findViewById(R.id.layoutoutto);
    }

    private void setDateTimeField() {
       // fromDateEtxt.setOnClickListener(this);
      //  toDateEtxt.setOnClickListener(this);
        layoutfrom.setOnClickListener(this);
        layoutto.setOnClickListener(this);


        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromdateval=dateFormatter.format(newDate.getTime());

                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
                FrameLayout fl = (FrameLayout) findViewById(R.id.frame);
                fl.removeAllViews();
                android.app.FragmentManager fragmentManager = getFragmentManager();

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragment_content hello = new fragment_content();
                fragmentTransaction.add(R.id.frame, hello, "HELLO");
                fragmentTransaction.commit();


            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                todateval=dateFormatter.format(newDate.getTime());
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
                FrameLayout fl = (FrameLayout) findViewById(R.id.frame);
                fl.removeAllViews();
                android.app.FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragment_content hello = new fragment_content();
                fragmentTransaction.add(R.id.frame, hello, "HELLO");
                fragmentTransaction.commit();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //    getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view == layoutfrom) {
            fromDatePickerDialog.show();
        } else if (view == layoutto) {
            toDatePickerDialog.show();
        } else if (view == newprojbtn) {
            Intent i = new Intent(this,New_.class);
             startActivity(i);
        }
//
    }

  /*  public void upload() {
        upload.setOnClickListener(this);


    } */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent i=new Intent();
        Bitmap bitmap= (Bitmap) data.getExtras().get("data");

        projectpk = common.currentitem.getPk();
        Toast.makeText(getApplicationContext(), "pk" + projectpk, Toast.LENGTH_SHORT).show();

        projectpk=common.currentitem.getPk();

       // bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

       // String projkey=(String) data.getExtras().get("projectpkey");
        i.setClass(this,Upload.class);
        i.putExtra("data",destination.getAbsolutePath().toString());
        i.putExtra("bitmap",bitmap);
        i.putExtra("projectpkey",projectpk);
        startActivity(i);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            }
        }
    }

    public void getproject(){

        client.get( Backendserver.url+ "/api/projects/project/?user=1",new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);

                //        Toast.makeText(getApplicationContext(),"res "+response.toString(),Toast.LENGTH_SHORT).show();
                for (int i = 0; i < response.length(); i++) {

                    JSONObject object=null;

                    try {
                        object = response.getJSONObject(i);

                        Log.e("errror"," "+object);


                        list.add(new horizontal_item(object));

                        Log.e("error"," "+list.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                listshow.setHasFixedSize(true);
                listshow.setLayoutManager(new LinearLayoutManager(MainActivity.this,LinearLayoutManager.HORIZONTAL,false));
                adapter = new horizontalviewadapter(list, MainActivity.this);
                listshow.setAdapter(adapter);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getApplicationContext(),"data not fetching"+errorResponse,Toast.LENGTH_SHORT);
            }
        });

    }

}