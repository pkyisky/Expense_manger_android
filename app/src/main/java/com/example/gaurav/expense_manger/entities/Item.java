package com.example.gaurav.expense_manger.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class Item {



    JSONObject object;

    public Item(JSONObject object) throws JSONException {
        this.object = object;
        this.pk = object.getString("pk");

        this.title1 = object.getString("title");

    }



    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String pk;
    public String title1;
}
