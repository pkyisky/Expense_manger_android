package com.example.gaurav.expense_manger;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.example.gaurav.expense_manger.Adapter.horizontalviewadapter;
import com.example.gaurav.expense_manger.Backend.Backendserver;
import com.example.gaurav.expense_manger.entities.horizontal_item;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;


public class fragment_content extends Fragment {

    private static MainActivity mActivity;
    private String time,deliveredcount,cod,totals,ongoingcount,cards;
    BarChart chart;
    float barWidth;
    float barSpace;
    float groupSpace;
    XAxis xAxis;
    ArrayList yVals1;
    String fromdate,toodate,defaultfrom,defaultto;

    ArrayList xVals;
    ArrayList arrayList;
    private TextView tvContent,order,ongoings,codtext,cardtext,totaltext;
    AsyncHttpClient client;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content, container, false);
        // getreport();

        mActivity = (MainActivity) getActivity();
        arrayList=new ArrayList();
       // tvContent = (TextView) view.findViewById(R.id.tvContent);
        Backendserver backend = new Backendserver(mActivity);
        client = backend.getHTTPClient();
        chart = (BarChart)view.findViewById(R.id.barchart);
        chart.setDescription(null);

      //float a=1.6f;
        fromdate=mActivity.fromdateval;
        toodate=mActivity.todateval;
        defaultfrom="01-"+mActivity.defaultfrmdate;
        defaultto=mActivity.defaulttodate;
      //Toast.makeText(mActivity,""+defaultto,Toast.LENGTH_SHORT).show();
      //Toast.makeText(mActivity,""+defaultfrom ,Toast.LENGTH_SHORT).show();
        Log.e("error",""+fromdate);
        Log.e("error",""+defaultfrom);

      //chart.setPinchZoom(true);
        chart.zoomIn();
        chart.setScaleMinima(2f, 1.2f);

      //chart.setScaleEnabled(false);
      //chart.setDrawBarShadow(false);
      //chart.setDrawGridBackground(false);

        barWidth = 0.3f;
        barSpace = 0f;
        groupSpace = 0.32f;

      //int groupCount = 6;
        xVals = new ArrayList();
        yVals1 = new ArrayList();


        if(fromdate==null&&toodate==null&&defaultfrom!=null&&defaultto!=null)
        {
            Log.e("error","1");
            client.get( Backendserver.url+ "/api/projects/project/?projectClosed=false&from="+defaultfrom+"&to="+defaultto,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);


                    for (int i = 0; i < response.length(); i++) {

                        JSONObject object=null;

                        try {
                            object = response.getJSONObject(i);


                            String name = object.getString("title");

                            Float bar= Float.valueOf(object.getString("totalCost"));


                            xVals.add(name);
                            // xVals.add(bar);

                            xAxis = chart.getXAxis();
                            xAxis.setGranularity(1f);
                            xAxis.setGranularityEnabled(true);
                            xAxis.setCenterAxisLabels(false);
                            xAxis.setDrawGridLines(false);
                            xAxis.setAxisMaximum(xVals.size());
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                            xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
                            Log.e("error"," "+xVals.toString());



                            yVals1.add(new BarEntry(i, (float) bar));

                            BarDataSet set1, set2;
                            set1 = new BarDataSet(yVals1, "Project");

                            set1.setColors(ColorTemplate.COLORFUL_COLORS);

                            // set1.setColor(Color.RED);
                            //  set2 = new BarDataSet(yVals2, "B");
                            // set2.setColor(Color.BLUE);
                            BarData data = new BarData(set1);
                            data.setValueFormatter(new LargeValueFormatter());
                            data.setValueTextSize(11f);
                            chart.setData(data);

                            chart.getBarData().setBarWidth(barWidth);

                            //chart.getXAxis().setAxisMinimum(0);
                            // chart.getXAxis().setAxisMaximum(0 + chart.getBarData().getGroupWidth(groupSpace,barSpace) * groupCount);
                            //   chart.groupBars(0, groupSpace, barSpace);
                            chart.setFitBars(true);
                            chart.getData().setHighlightEnabled(false);
                            chart.invalidate();
                            Legend l = chart.getLegend();
                            l.setEnabled(false);

                            chart.getAxisRight().setEnabled(false);
                            YAxis leftAxis = chart.getAxisLeft();
                            leftAxis.setValueFormatter(new LargeValueFormatter());
                            leftAxis.setDrawGridLines(true);
                            leftAxis.setSpaceTop(40f);

                            leftAxis.setAxisMinimum(0f);
                            // leftAxis.setValueFormatter(new IndexAxisValueFormatter(yVals));
                            chart.animateY(2000);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toast.makeText(mActivity,"data not fetching"+errorResponse,Toast.LENGTH_SHORT);
                }
            });

        }else if(fromdate!=null&&toodate!=null&&defaultfrom!=null&&defaultto!=null)
        {
            Log.e("error","2");
            client.get( Backendserver.url+ "/api/projects/project/?projectClosed=false&from="+fromdate+"&to="+toodate,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);


                    for (int i = 0; i < response.length(); i++) {

                        JSONObject object=null;

                        try {
                            object = response.getJSONObject(i);


                            String name = object.getString("title");

                            Float bar= Float.valueOf(object.getString("totalCost"));


                            xVals.add(name);
                            // xVals.add(bar);

                            xAxis = chart.getXAxis();
                            xAxis.setGranularity(1f);
                            xAxis.setGranularityEnabled(true);
                            xAxis.setCenterAxisLabels(false);
                            xAxis.setDrawGridLines(false);
                            xAxis.setAxisMaximum(xVals.size());
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                            xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
                            Log.e("error"," "+xVals.toString());



                            yVals1.add(new BarEntry(i, (float) bar));

                            BarDataSet set1, set2;
                            set1 = new BarDataSet(yVals1, "Project");

                            set1.setColors(ColorTemplate.COLORFUL_COLORS);

                            //set1.setColor(Color.RED);
                            //set2 = new BarDataSet(yVals2, "B");
                            //set2.setColor(Color.BLUE);
                            BarData data = new BarData(set1);
                            data.setValueFormatter(new LargeValueFormatter());
                            data.setValueTextSize(11f);
                            chart.setData(data);

                            chart.getBarData().setBarWidth(barWidth);

                            //chart.getXAxis().setAxisMinimum(0);
                            //chart.getXAxis().setAxisMaximum(0 + chart.getBarData().getGroupWidth(groupSpace,barSpace) * groupCount);
                            //chart.groupBars(0, groupSpace, barSpace);
                            chart.setFitBars(true);
                            chart.getData().setHighlightEnabled(false);
                            chart.invalidate();
                            Legend l = chart.getLegend();
                            l.setEnabled(false);

                            chart.getAxisRight().setEnabled(false);
                            YAxis leftAxis = chart.getAxisLeft();
                            leftAxis.setValueFormatter(new LargeValueFormatter());
                            leftAxis.setDrawGridLines(true);
                            leftAxis.setSpaceTop(40f);

                            leftAxis.setAxisMinimum(0f);
                            // leftAxis.setValueFormatter(new IndexAxisValueFormatter(yVals));
                            chart.animateY(2000);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toast.makeText(mActivity,"data not fetching"+errorResponse,Toast.LENGTH_SHORT);
                }
            });
        }else if(fromdate!=null&&toodate==null&&defaultfrom!=null&&defaultto!=null)
        {
            Log.e("error","3");
            client.get( Backendserver.url+ "/api/projects/project/?projectClosed=false&from="+fromdate+"&to="+defaultto,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);


                    for (int i = 0; i < response.length(); i++) {

                        JSONObject object=null;

                        try {
                            object = response.getJSONObject(i);


                            String name = object.getString("title");

                            Float bar= Float.valueOf(object.getString("totalCost"));


                            xVals.add(name);
                            // xVals.add(bar);

                            xAxis = chart.getXAxis();
                            xAxis.setGranularity(1f);
                            xAxis.setGranularityEnabled(true);
                            xAxis.setCenterAxisLabels(false);
                            xAxis.setDrawGridLines(false);
                            xAxis.setAxisMaximum(xVals.size());
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                            xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
                            Log.e("error"," "+xVals.toString());



                            yVals1.add(new BarEntry(i, (float) bar));

                            BarDataSet set1, set2;
                            set1 = new BarDataSet(yVals1, "Project");

                            set1.setColors(ColorTemplate.COLORFUL_COLORS);

                            // set1.setColor(Color.RED);
                            //  set2 = new BarDataSet(yVals2, "B");
                            // set2.setColor(Color.BLUE);
                            BarData data = new BarData(set1);
                            data.setValueFormatter(new LargeValueFormatter());
                            data.setValueTextSize(11f);
                            chart.setData(data);

                            chart.getBarData().setBarWidth(barWidth);

                            //chart.getXAxis().setAxisMinimum(0);
                            // chart.getXAxis().setAxisMaximum(0 + chart.getBarData().getGroupWidth(groupSpace,barSpace) * groupCount);
                            //   chart.groupBars(0, groupSpace, barSpace);
                            chart.setFitBars(true);
                            chart.getData().setHighlightEnabled(false);
                            chart.invalidate();
                            Legend l = chart.getLegend();
                            l.setEnabled(false);

                            chart.getAxisRight().setEnabled(false);
                            YAxis leftAxis = chart.getAxisLeft();
                            leftAxis.setValueFormatter(new LargeValueFormatter());
                            leftAxis.setDrawGridLines(true);
                            leftAxis.setSpaceTop(40f);

                            leftAxis.setAxisMinimum(0f);
                            // leftAxis.setValueFormatter(new IndexAxisValueFormatter(yVals));
                            chart.animateY(2000);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toast.makeText(mActivity,"data not fetching"+errorResponse,Toast.LENGTH_SHORT);
                }
            });
        }else if(fromdate==null&&toodate!=null&&defaultfrom!=null&&defaultto!=null) {
            Log.e("error","4");
            client.get(Backendserver.url + "/api/projects/project/?projectClosed=false&from=" + defaultfrom + "&to=" + toodate, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);


                    for (int i = 0; i < response.length(); i++) {

                        JSONObject object = null;

                        try {
                            object = response.getJSONObject(i);


                            String name = object.getString("title");

                            Float bar = Float.valueOf(object.getString("totalCost"));


                            xVals.add(name);
                            // xVals.add(bar);

                            xAxis = chart.getXAxis();
                            xAxis.setGranularity(1f);
                            xAxis.setGranularityEnabled(true);
                            xAxis.setCenterAxisLabels(false);
                            xAxis.setDrawGridLines(false);
                            xAxis.setAxisMaximum(xVals.size());
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                            xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
                            Log.e("error", " " + xVals.toString());


                            yVals1.add(new BarEntry(i, (float) bar));

                            BarDataSet set1, set2;
                            set1 = new BarDataSet(yVals1, "Project");

                            set1.setColors(ColorTemplate.COLORFUL_COLORS);

                            // set1.setColor(Color.RED);
                            //  set2 = new BarDataSet(yVals2, "B");
                            // set2.setColor(Color.BLUE);
                            BarData data = new BarData(set1);
                            data.setValueFormatter(new LargeValueFormatter());
                            data.setValueTextSize(11f);
                            chart.setData(data);

                            chart.getBarData().setBarWidth(barWidth);

                            //chart.getXAxis().setAxisMinimum(0);
                            // chart.getXAxis().setAxisMaximum(0 + chart.getBarData().getGroupWidth(groupSpace,barSpace) * groupCount);
                            //   chart.groupBars(0, groupSpace, barSpace);
                            chart.setFitBars(true);
                            chart.getData().setHighlightEnabled(false);
                            chart.invalidate();
                            Legend l = chart.getLegend();
                            l.setEnabled(false);

                            chart.getAxisRight().setEnabled(false);
                            YAxis leftAxis = chart.getAxisLeft();
                            leftAxis.setValueFormatter(new LargeValueFormatter());
                            leftAxis.setDrawGridLines(true);
                            leftAxis.setSpaceTop(40f);

                            leftAxis.setAxisMinimum(0f);
                            // leftAxis.setValueFormatter(new IndexAxisValueFormatter(yVals));
                            chart.animateY(2000);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toast.makeText(mActivity, "data not fetching" + errorResponse, Toast.LENGTH_SHORT);
                }
            });
        }






        ArrayList yVals = new ArrayList();

        yVals.add("1000");
        yVals.add("2000");
        yVals.add("3000");
        yVals.add("4000");
        yVals.add("5000");
        yVals.add("6000");







//        order.setText(deliveredcount);




       /* ArrayList NoOfEmp = new ArrayList();

        NoOfEmp.add(new BarEntry(945f, 0));
        NoOfEmp.add(new BarEntry(1040f, 1));
        NoOfEmp.add(new BarEntry(1133f, 2));
        NoOfEmp.add(new BarEntry(1240f, 3));
        NoOfEmp.add(new BarEntry(1369f, 4));
        NoOfEmp.add(new BarEntry(1487f, 5));
        NoOfEmp.add(new BarEntry(1501f, 6));
        NoOfEmp.add(new BarEntry(1645f, 7));
        NoOfEmp.add(new BarEntry(1578f, 8));
        NoOfEmp.add(new BarEntry(1695f, 9));

        ArrayList year = new ArrayList();

        year.add("2008");
        year.add("2009");
        year.add("2010");
        year.add("2011");
        year.add("2012");
        year.add("2013");
        year.add("2014");
        year.add("2015");
        year.add("2016");
        year.add("2017");

        BarDataSet bardataset = new BarDataSet(NoOfEmp, "No Of Employee");
        chart.animateY(5000);
        BarData data = new BarData(bardataset);
        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        chart.setData(data); */

        return view;
    }
    private void startCountAnimation() {
        ValueAnimator animator = ValueAnimator.ofInt(0, 50);
        animator.setDuration(5000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                order.setText(animation.getAnimatedValue().toString());
            }
        });
        animator.start();
    }

    public void expensegraph(){

        chart.setDescription(null);

        //float a=1.6f;

        //  chart.setPinchZoom(true);
        chart.zoomIn();
        chart.setScaleMinima(2.8f, 1.4f);

        //   chart.setScaleEnabled(false);
        //   chart.setDrawBarShadow(false);
        //   chart.setDrawGridBackground(false);

        barWidth = 0.35f;
        barSpace = 0f;
        groupSpace = 0.32f;

        int groupCount = 15;

       final ArrayList xVals = new ArrayList();
        client.get( Backendserver.url+ "/api/projects/project/",new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);

                //        Toast.makeText(getApplicationContext(),"res "+response.toString(),Toast.LENGTH_SHORT).show();
                for (int i = 0; i < response.length(); i++) {

                    JSONObject object=null;

                    try {
                        object = response.getJSONObject(i);

                        //   JSONObject service=object.getJSONObject("service");
                         String name = object.getString("title");
                        //   String mobile = object.getString("description");
                      /*  JSONArray schedule_Array = object.getJSONArray("service");
                        for (int j=0;j<schedule_Array.length();j++)
                        {
                            JSONObject jOBJNEW = schedule_Array.getJSONObject(j);
                            Toast.makeText(getApplicationContext(),"res "+jOBJNEW.toString(),Toast.LENGTH_SHORT).show();
                            Grnlists.add(new Grn_item(jOBJNEW));
                        }*/

                        // Toast.makeText(getApplicationContext(),"res "+object.toString(),Toast.LENGTH_SHORT).show();
                        //   object.put("title",name);
                        // object.put("description",mobile);
                        Log.e("errror"," "+object);
                        // Item item = new Item(object);

                        xVals.add(name);
                        //       Toast.makeText(getApplicationContext(),"res "+productlists.toString()+" "+productlists.size(),Toast.LENGTH_SHORT).show();
                        Log.e("error"," "+xVals.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                //    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(mActivity,"data not fetching"+errorResponse,Toast.LENGTH_SHORT);
            }
        });


      /*  xVals.add("Project 1");
        xVals.add("Project 2");
        xVals.add("Project 3");
        xVals.add("Project 4");
        xVals.add("Project 5");
        xVals.add("Project 6");
        xVals.add("Project 7");
        xVals.add("Project 8");
        xVals.add("Project 9");
        xVals.add("Project 10");
        xVals.add("Project 11");
        xVals.add("Project 12");
        xVals.add("Project 9");
        xVals.add("Project 10");
        xVals.add("Project 11");
        xVals.add("Project 12");*/


        ArrayList yVals = new ArrayList();

        yVals.add("1000");
        yVals.add("2000");
        yVals.add("3000");
        yVals.add("4000");
        yVals.add("5000");
        yVals.add("6000");
        yVals.add("7000");
        yVals.add("8000");
        yVals.add("9000");
        yVals.add("10000");
        yVals.add("11000");
        yVals.add("12000");
        yVals.add("13000");
        yVals.add("14000");
        yVals.add("15000");




        ArrayList yVals1 = new ArrayList();
        ArrayList yVals2 = new ArrayList();

        yVals1.add(new BarEntry(1, (float) 1000));

        yVals1.add(new BarEntry(2, (float) 2000));

        yVals1.add(new BarEntry(3, (float) 1000));

        yVals1.add(new BarEntry(4, (float) 4000));

        yVals1.add(new BarEntry(5, (float) 8000));

        yVals1.add(new BarEntry(6, (float) 6000));

     //   yVals1.add(new BarEntry(7, (float) 7000));



        BarDataSet set1, set2;
        set1 = new BarDataSet(yVals1, "Project");

        set1.setColors(ColorTemplate.COLORFUL_COLORS);

        // set1.setColor(Color.RED);
        set2 = new BarDataSet(yVals2, "B");
        set2.setColor(Color.BLUE);
        BarData data = new BarData(set1, set2);
        data.setValueFormatter(new LargeValueFormatter());
        chart.setData(data);
        chart.getBarData().setBarWidth(barWidth);
       // chart.getXAxis().setAxisMinimum(0);
      //  chart.getXAxis().setAxisMaximum(0 + chart.getBarData().getGroupWidth(groupSpace,barSpace) * groupCount);
      //  chart.groupBars(0, groupSpace, barSpace);
        chart.getData().setHighlightEnabled(false);
        chart.invalidate();
        Legend l = chart.getLegend();
        l.setEnabled(false);

      /*  Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(true);
        l.setYOffset(20f);
        l.setXOffset(0f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f); */

//X-axis
        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);


        xAxis.setAxisMaximum(16);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
//Y-axis
        chart.getAxisRight().setEnabled(false);
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(35f);

        leftAxis.setAxisMinimum(0f);
        // leftAxis.setValueFormatter(new IndexAxisValueFormatter(yVals));
        chart.animateY(2000);

    }



    }


