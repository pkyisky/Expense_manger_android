package com.example.gaurav.expense_manger;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.example.gaurav.expense_manger.Backend.Backendserver;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class New_ extends AppCompatActivity implements View.OnClickListener {
    EditText projecttitle,budget,desciption;
    AsyncHttpClient client;
    private TextView duedatetv;
    LinearLayout date_layout;
    Button submit;
    String duedate;
    TextView errornewtitle,errornewbudget,errornewdate,errornewdesc;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_);
        Backendserver backend = new Backendserver(getApplicationContext());
        client = backend.getHTTPClient();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        findViewsById();
        setDateTimeField();


        submit.setOnClickListener(this);

    }


    private void findViewsById() {
         duedatetv = findViewById(R.id.duedateet);
         projecttitle =findViewById(R.id.projtitle);
         budget=findViewById(R.id.projbudget);
         desciption=findViewById(R.id.projdesc);
         submit=findViewById(R.id.submitprojbtn);

        errornewtitle=findViewById(R.id.errornewtitle);
        errornewbudget=findViewById(R.id.errornewbudget);
        errornewdate=findViewById(R.id.errornewdate);
        errornewdesc=findViewById(R.id.errornewdesc);


//        duedatetv.setInputType(InputType.TYPE_NULL);

    //    duedatetv.requestFocus();


        date_layout=this.findViewById(R.id.due_datelayout);

    }



    private void setDateTimeField() {
        // fromDateEtxt.setOnClickListener(this);
        //  toDateEtxt.setOnClickListener(this);
        date_layout.setOnClickListener(this);


        Calendar newCalendar = Calendar.getInstance();

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                duedate=dateFormatter.format(newDate.getTime());
                duedatetv.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //    getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view == date_layout) {
           toDatePickerDialog .show();
        } else if(view==submit){
            projectpost();;
        }
    }


public void projectpost(){
        if(projecttitle.length()==0&&budget.length()==0&&desciption.length()==0&&duedatetv.length()==0){
        errornewtitle.setVisibility(View.VISIBLE);
        errornewdate.setVisibility(View.VISIBLE);
        errornewdesc.setVisibility(View.VISIBLE);
        errornewbudget.setVisibility(View.VISIBLE);

        errornewtitle.setTextColor(Color.parseColor("#ff0000"));
        errornewtitle.setText("Title is required.");
        errornewdate.setTextColor(Color.parseColor("#ff0000"));
        errornewdate.setText("Due date is required.");
        errornewbudget.setTextColor(Color.parseColor("#ff0000"));
        errornewbudget.setText("Budget is required.");
        errornewdesc.setTextColor(Color.parseColor("#ff0000"));
        errornewdesc.setText("Description is required.");

        //duedatetv.setError("Enter duedatetv");

    }

        else if(duedatetv.length()==0&&budget.length()==0&&desciption.length()==0&&projecttitle.length()>0){
            errornewtitle.setVisibility(View.GONE);

            errornewdesc.setVisibility(View.VISIBLE);
            errornewbudget.setVisibility(View.VISIBLE);
            errornewdate.setVisibility(View.VISIBLE);

            errornewdate.setTextColor(Color.parseColor("#ff0000"));
            errornewdate.setText("Due date is required.");
            errornewbudget.setTextColor(Color.parseColor("#ff0000"));
            errornewbudget.setText("Budget is required.");
            errornewdesc.setTextColor(Color.parseColor("#ff0000"));
            errornewdesc.setText("Description is required.");
            //duedatetv.setError("Enter duedatetv");

        }
        else if(duedatetv.length()==0&&budget.length()==0&&desciption.length()>0&&projecttitle.length()>0){
            errornewtitle.setVisibility(View.GONE);
            errornewdesc.setVisibility(View.GONE);

            errornewbudget.setVisibility(View.VISIBLE);
            errornewdate.setVisibility(View.VISIBLE);

            errornewdate.setTextColor(Color.parseColor("#ff0000"));
            errornewdate.setText("Due date is required.");
            errornewbudget.setTextColor(Color.parseColor("#ff0000"));
            errornewbudget.setText("Budget is required.");

        }
        else if(duedatetv.length()==0&&desciption.length()==0&&budget.length()>0&&projecttitle.length()>0){
            errornewtitle.setVisibility(View.GONE);
            errornewbudget.setVisibility(View.GONE);

            errornewdesc.setVisibility(View.VISIBLE);
            errornewdate.setVisibility(View.VISIBLE);

            errornewdate.setTextColor(Color.parseColor("#ff0000"));
            errornewdate.setText("Due date is required.");
            errornewdesc.setTextColor(Color.parseColor("#ff0000"));
            errornewdesc.setText("Description is required.");

        }
        else if(duedatetv.length()==0&&projecttitle.length()==0&&desciption.length()>0&&budget.length()>0){
            errornewdesc.setVisibility(View.GONE);
            errornewbudget.setVisibility(View.GONE);

            errornewtitle.setVisibility(View.VISIBLE);
            errornewdate.setVisibility(View.VISIBLE);

            errornewdate.setTextColor(Color.parseColor("#ff0000"));
            errornewdate.setText("Due date is required.");
            errornewtitle.setTextColor(Color.parseColor("#ff0000"));
            errornewtitle.setText("Title is required.");

        }
   else  if(projecttitle.length()==0)
    {
        errornewdesc.setVisibility(View.GONE);
        errornewbudget.setVisibility(View.GONE);
        errornewdate.setVisibility(View.GONE);
        errornewtitle.setVisibility(View.VISIBLE);

        errornewtitle.setTextColor(Color.parseColor("#ff0000"));
        errornewtitle.setText("Title is required.");
        // projecttitle.setError("Enter title");

    }else if(budget.length()==0){
            errornewdesc.setVisibility(View.GONE);
            errornewtitle.setVisibility(View.GONE);
            errornewdate.setVisibility(View.GONE);
            errornewbudget.setVisibility(View.VISIBLE);

            errornewbudget.setTextColor(Color.parseColor("#ff0000"));
            errornewbudget.setText("Budget is required.");
            // budget.setError("Enter budget");

        }else if(desciption.length()==0){
            errornewbudget.setVisibility(View.GONE);
            errornewtitle.setVisibility(View.GONE);
            errornewdate.setVisibility(View.GONE);

            errornewdesc.setVisibility(View.VISIBLE);
            errornewdesc.setTextColor(Color.parseColor("#ff0000"));
            errornewdesc.setText("Description is required.");
            // desciption.setError("Enter desciption");

        }else if(duedatetv.length()==0){
                errornewbudget.setVisibility(View.GONE);
                errornewtitle.setVisibility(View.GONE);
                errornewtitle.setVisibility(View.GONE);
            errornewdate.setVisibility(View.VISIBLE);
            errornewdate.setTextColor(Color.parseColor("#ff0000"));
            errornewdate.setText("Due date is required.");
            //duedatetv.setError("Enter duedatetv");

        }


        else if(projecttitle.length()>0&&budget.length()>0&&desciption.length()>0&&duedatetv.length()>0){
            RequestParams params=new RequestParams();
            params.put("title",projecttitle.getText().toString());
            params.put("budget",Integer.parseInt(budget.getText().toString()));
            params.put("description",desciption.getText().toString());
            params.put("dueDate",duedate);
            //params.put("dueDate","2019-01-17T05:05:20.285000Z");
            Log.e("error",""+params);
            client.post(Backendserver.url + "/api/projects/project/",params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Toast.makeText(getApplicationContext(),"Project added",Toast.LENGTH_SHORT).show();

                    Intent i=new Intent();
                    i.setClass(getApplicationContext(),MainActivity.class);
                    startActivity(i);
                    finish();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(getApplicationContext(),"unsuccess ",Toast.LENGTH_SHORT).show();
                    Log.e("error",""+error);

                }
            });


        }





}


}
