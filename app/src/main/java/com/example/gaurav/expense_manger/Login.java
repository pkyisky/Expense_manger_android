package com.example.gaurav.expense_manger;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gaurav.expense_manger.Backend.Backendserver;
import com.example.gaurav.expense_manger.Backend.SessionManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.cookie.Cookie;

public class Login extends AppCompatActivity {
   Context mContext;
    EditText user, pass;
    Button login,otp;
    ImageView pencilss;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    TextInputLayout tilUser, tilPass;
    LinearLayout llUsername, llPassword;
    String mobileStr;

    TextView resendotp,erroruser,errorotp;
    Context context;
    private CookieStore httpCookieStore;
    private AsyncHttpClient client;
    SessionManager sessionManager;
    String csrfId, sessionId;

    String TAG = "status";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = Login.this;
        user = (EditText) findViewById(R.id.mobile);
        pass = (EditText) findViewById(R.id.otp);
        otp =  findViewById(R.id.get_otp);
        pencilss =  findViewById(R.id.pencil);
        resendotp=findViewById(R.id.resendotp);
        login = (Button) findViewById(R.id.sign_in_button);
        tilUser = findViewById(R.id.til_user);
        tilPass = findViewById(R.id.til_password);
        llUsername = findViewById(R.id.llUsername);
        llPassword = findViewById(R.id.llPassword);
        erroruser=findViewById(R.id.error);
        errorotp=findViewById(R.id.errorotp);

        httpCookieStore = new PersistentCookieStore(this);
        httpCookieStore.clear();
        // client = new AsyncHttpClient();
        Backendserver backend = new Backendserver(getApplicationContext());
        client = backend.getHTTPClient();
       // client=new AsyncHttpClient();
        client.setCookieStore(httpCookieStore);
         resendotp.setVisibility(View.GONE);
        pencilss.setVisibility(View.GONE);
      //  isStoragePermissionGranted();
        sessionManager = new SessionManager(getApplicationContext());
         llPassword.setVisibility(View.GONE);
         login.setVisibility(View.GONE);
       //  erroruser.setVisibility(View.GONE);
      //   erroruser.setVisibility(View.GONE);


        otp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                login();

            }
        });
         pencilss.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 login.setVisibility(View.GONE);
                 otp.setVisibility(View.VISIBLE);
                 user.setEnabled(true);
                 llPassword.setVisibility(View.GONE);
                 resendotp.setVisibility(View.GONE);
                 errorotp.setVisibility(View.GONE);
             }
         });

        resendotp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                resendotp.setVisibility(View.VISIBLE);
                resndotp();

            }
        });
    }

    public void register(View view) {
        //   Intent i = new Intent(getApplicationContext(), Registration.class);
        //  startActivity(i);
    }
    public void resndotp(){
        Toast.makeText(this, Backendserver.url, Toast.LENGTH_LONG).show();
        mobileStr = user.getText().toString();
        if (mobileStr.isEmpty()){
            erroruser.setTextColor(Color.parseColor("#ff0000"));
            erroruser.setText("Username is required.");
            user.requestFocus();
        } else {
            tilUser.setErrorEnabled(false);
            RequestParams params = new RequestParams();
            params.put("id",mobileStr);
            Log.e("login",mobileStr);

            client.post(mContext,Backendserver.url+"/generateOTP", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.e("login",response.toString());

                    Toast.makeText(Login.this, "Resending otp", Toast.LENGTH_SHORT).show();
                    login.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loginFromOTP(mobileStr);
                        }
                    });
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Toast.makeText(Login.this, "onFailure", Toast.LENGTH_SHORT).show();
                    Log.e("login",responseString+"  "+throwable.toString());
                    Log.e("login",mobileStr);
                }
            });
        }
    }


    public void login(){
        Toast.makeText(this, Backendserver.url, Toast.LENGTH_LONG).show();
        mobileStr = user.getText().toString();
        if (mobileStr.isEmpty()){
           // tilUser.setErrorEnabled(true);
           // tilUser.setError("Username is required");
            erroruser.setTextColor(Color.parseColor("#ff0000"));
            erroruser.setText("Username is required.");
            user.requestFocus();
        } else {
            tilUser.setErrorEnabled(false);
            RequestParams params = new RequestParams();
            params.put("id",mobileStr);
            Log.e("login",mobileStr);

            client.post(mContext,Backendserver.url+"/generateOTP", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.e("login",response.toString());
                    user.setEnabled(false);
                    llPassword.setVisibility(View.VISIBLE);
                    login.setVisibility(View.VISIBLE);
                    otp.setVisibility(View.GONE);
                    resendotp.setVisibility(View.VISIBLE);
                    pencilss.setVisibility(View.VISIBLE);
                    erroruser.setVisibility(View.GONE);
                 // getSmsOTP();
                    login.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loginFromOTP(mobileStr);
                        }
                    });
                    Toast.makeText(Login.this, "getting otp", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Toast.makeText(Login.this, "onFailure", Toast.LENGTH_SHORT).show();
                    Log.e("login",responseString+"  "+throwable.toString());
                    Log.e("login",mobileStr);
                }
            });
        }
    }



    public void loginFromOTP(String mob){
        Toast.makeText(this, Backendserver.url, Toast.LENGTH_LONG).show();
        String mobOtp = pass.getText().toString();
        if (mobOtp.isEmpty()){
           // tilPass.setErrorEnabled(true);
           // tilPass.setError("Mobile OTP is required.");
            errorotp.setTextColor(Color.parseColor("#ff0000"));
            errorotp.setText("Mobile OTP is required.");
            pass.requestFocus();
        } else {
            tilPass.setErrorEnabled(false);
            csrfId = sessionManager.getCsrfId();
            sessionId = sessionManager.getSessionId();
            if (csrfId.equals("") && sessionId.equals("")) {
                RequestParams params = new RequestParams();
                params.put("username", mob);
                params.put("otp", mobOtp);
                client.post(Backendserver.url + "/login?next=/", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject c) {
                        Log.e("LoginActivity", "  onSuccess");
                        super.onSuccess(statusCode, headers, c);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject c) {
                        super.onFailure(statusCode, headers, e, c);
                        if (statusCode == 401) {
                            Toast.makeText(mContext, "unsuccess", Toast.LENGTH_SHORT).show();
                            Log.e("LoginActivity", "  onFailure login");
                        }
                    }

                    @Override
                    public void onFinish() {

                        List<Cookie> lst = httpCookieStore.getCookies();
                        if (lst.isEmpty()) {
                            //  Toast.makeText(Login.this, String.format("Error , Empty login activity cookie store"), Toast.LENGTH_SHORT).show();
                            Log.e("LoginActivity", "Empty cookie store");
                        } else {
                            if (lst.size() < 2) {
                                String msg = String.format("Error while logining, fetal error!");
                                Toast.makeText(Login.this, msg, Toast.LENGTH_SHORT).show();
                                Log.e("LoginActivity", ""+msg);
                                return;
                            }

                            Cookie csrfCookie = lst.get(0);
                            Cookie sessionCookie = lst.get(1);

                            String csrf_token = csrfCookie.getValue();
                            String session_id = sessionCookie.getValue();
                            sessionManager.setCsrfId(csrf_token);
                            sessionManager.setSessionId(session_id);
                            sessionManager.setUsername(user.getText().toString());

                            startActivity(new Intent(Login.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();


                        }
                        Log.e("LoginActivity", "  finished");


                    }
                });
            }
        }
    }

    @Override
    protected void onDestroy() {
        Intent intent = new Intent("com.cioc.libreerp.backendservice");
        sendBroadcast(intent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();
    }



    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.PROCESS_OUTGOING_CALLS) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.CALL_PHONE ,
                        android.Manifest.permission.READ_PHONE_STATE , android.Manifest.permission.PROCESS_OUTGOING_CALLS,
                        Manifest.permission.SEND_SMS}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int i = 1; i < 6; i++) {
            if (requestCode == i){
                if (grantResults.length > 0
                        && grantResults[i-1] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG, "Permission: " + permissions[i-1] + "was " + grantResults[i-1]);
                    //resume tasks needing this permission
                }
                return;
            }
        }
    }
}




